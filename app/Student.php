<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model{
    protected $table = 'student';
    protected $fillable = ['name'];
    public $timestamps = true;

    protected function getDateFormat(){
        return time();
    }

    protected function asDateTime($val){
        return $val;
    }
}